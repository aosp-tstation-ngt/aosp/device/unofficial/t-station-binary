ifneq ($(filter t-station,$(TARGET_DEVICE)),)
LOCAL_PATH := $(call my-dir)


##########################
# kernel
##########################

include $(CLEAR_VARS)
LOCAL_MODULE := tstation-kernel
LOCAL_MODULE_STEM := kernel
LOCAL_SRC_FILES := zImage
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := OTHER
LOCAL_MODULE_PATH := $(PRODUCT_OUT)

include $(BUILD_PREBUILT)


##########################
# resource
##########################

include $(CLEAR_VARS)
LOCAL_MODULE := tstation-resource
LOCAL_MODULE_STEM := resource.img
LOCAL_SRC_FILES := resource.img
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := OTHER
LOCAL_MODULE_PATH := $(PRODUCT_OUT)

include $(BUILD_PREBUILT)


##########################
# uboot
##########################

include $(CLEAR_VARS)
LOCAL_MODULE := tstation-uboot
LOCAL_MODULE_STEM := uboot.img
LOCAL_SRC_FILES := uboot.img
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := OTHER
LOCAL_MODULE_PATH := $(PRODUCT_OUT)

include $(BUILD_PREBUILT)


endif

